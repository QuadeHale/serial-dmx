# serial-dmx

I was frustrated by how obfuscated and expensive DMX is. After trying (and paying) for several USB-DMX adapters, I realized I had the power to easily generate the DMX signalling myself with an off-the-shelf USB-RS485 for $15.

Libraries were bloated and/or antiquated and/or inflexible. I wanted something simple that was designed to be included in other modules, that had a minimum of dependencies. This module has only two.

Loosely based on the `node-dmx` library which can be found [here](https://github.com/wiedi/node-dmx).

## How to use
- Include the library
- Call the library with the serial port and the number of channels
  - If you declare less than 512 channels, you'll get a higher effective 'framerate', because DMX allows for that.
- Manipulate the channels with `universe.set()`
- Stop it with `universe.stop()`
- That's it!

```Javascript
const SerialDMX = require('serial-dmx')

let universe = SerialDMX('/dev/ttyUSB0', 48)
universe.set({1: 255}) // set channel 1 to max.
let x = setTimeout(() => {universe.stop()}, 1000)
```

### Options, Properties and Methods
#### SerialDMX(port, channelCount, autoStart) options
- `port` is something like `/dev/ttyUSB0`
- `channelCount` is optional. It defaults to 512, but can be set lower to get a higher effective framerate.
- `autoStart` is optional. It defaults to `true`.
- returns an instance of `SerialDMX.universe`

#### SerialDMX.universe properties
##### .isRunning
Is `true` if the universe is running. `false` otherwise.
##### .channelCount
How many channels the universe was created with.
##### .levels
The current levels.

#### SerialDMX.universe public methods
##### .start()
Opens the serial port. Will throw an error if the port can't open. Sets `.isRunning` to `true` and then returns whatever it is.
##### .stop()
Returns `undefined`. Closes the serial port.
##### .set(obj | arr)
You can set the channels with something like `{1: 210, 2: 98, 4: 77}` or send an array with the values, ala `[null, 210, 98, null, 77]`. Keep in mind array index 0 is always ignored. `undefined` and `null` both mean no change. Anything returning `isNaN()` is ignored. Any value greater than 255 is 'chopped' to 255, and anything less than zero is 'chopped' to 0.

#### SerialDMX.universe private methods/properties
