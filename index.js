'use strict'
const SerialPort = require('serialport')
const timer = require('nanotimer')

function SerialDMX(port, channelCount, autoStart) {
  if (!port) { throw new Error('SerialDMX cannot start without a port.')}
	if (!channelCount) { this.channelCount = 512 }
  if (!autoStart) { autoStart = true }

  this._port = port
	this._buffer = Buffer.alloc(channelCount + 1, 0)
  this._isRunning = false

	this._port = new SerialPort(this._port, {
		'baudRate': 250000,
		'dataBits': 8,
		'stopBits': 2,
		'parity': 'none'
	}, function(err) {
		if(err) {
			console.err(err)
			return
		}
		if (autoStart) {
      this.start()
    }
	})
}

SerialDMX._timerCalc = function () {
  // Calculate how long our frame is based on the buffer
  // so we don't overflow the buffer.
}

SerialDMX._sender = function() {
  //
}

SerialDMX.start = function() {
	//
}

SerialDMX.stop = function() {
	//)
}

SerialDMX.get = function() {
  return [...this._buffer]
}

SerialDMX.set = function() {
	//
}

module.exports = SerialDMX
